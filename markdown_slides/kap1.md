## 1. Ziele des Trainings

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/lzm.png" alt="">


Quelle: Petersen, Britta, Engelhardt, Claudia, Hörner, Tanja, Jacob, Juliane, Kvetnaya, Tatiana, Mühlichen, Andreas, Schranzhofer, Hermann, Schulz, Sandra, Slowig, Benjamin, Trautwein-Bruns, Ute, Voigt, Anne, & Wiljes, Cord. (2022). Lernzielmatrix zum Themenbereich Forschungsdatenmanagement (FDM) für die Zielgruppen Studierende, PhDs und Data Stewards (Version 1). Zenodo. https://doi.org/10.5281/zenodo.7034478



## 2. Warum ist Datenqualität für meine Forschung wichtig?

### Herausforderung Datenqualität

#### Kritische Betrachtung

* Die Qualität von Daten zeichnet sich unter anderem durch Transparenz und Nachvollziehbarkeit der Datensätze aus.

* Entsprechend der **FAIR***-Prinzipien sollten die Daten auffindbar (**findable**), zugänglich (**accessible**), interoperabel (**interoperable**) und wiederverwendbar (**reusable**) sein.

<br>
<br>
<br>
<br>
<br>

[**Mehr zur FAIR Data finden Sie im separaten Lernmodul "Metadaten"!*](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/Metadaten/html_slides/metadaten.html)


<img align="left" src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/kreislauf.png" alt="">
<img align="top" src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/legende_kreislauf.png" alt="">

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

RfII – Rat für Informationsinfrastrukturen (2019): Herausforderung Datenqualität – Empfehlungen zur Zukunftsfähigkeit von Forschung im digitalen Wandel, zweite Auflage, Göttingen.
https://rfii.de/download/herausforderung-datenqualitaet-november-2019/ 


### Bedeutung Datenqualität

#### Wo spielt die Datenqualität der Forschungsdaten eine Rolle?

* **Dokumentation** (z.B. DFG 10 Jahre für gute wissenschaftliche Praxis)
    * Nachvollziehbarkeit für Verantwortungszwecke (bspw. wissenschaftl. Institutionen)
    * Charakteristik: statische Daten, kein/seltener Zugriff, "dark archive", definierte Dauer

* **Nachnutzung/Publikation*** (z.B. Erklärungen in DFG-Anträgen seit 2010)
    * Zitierfähigkeit und erneute wissenschaftliche Nutzung
    * Bewahrung nicht reproduzierbarer Daten
    * Charakteristik: dauerhaft oder ohne klares Enddatum
    
* **verbesserte Datennutzung in Projekten** (bspw. Forschergruppen/Verbünde - z.T. INF-Projekte in SFBs)
    * Datennutzung erleichtern und neue Methoden/Funktionen ermöglichen
    * Verknüpfung mit kollaborativen Forschungsumgebungen und Werkzeugen
    * nach Ende des Forschungsprojektes Übergang in andere Kontexte
    * Charakteristik: Daten werden ständig benutzt und verändern sich

**Mehr zu den Themen ["Nachnutzung"](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dlc-datalifecycle/html_slides/dlc6.html#/) und ["Publikation"](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dlc-datalifecycle/html_slides/dlc4.html#/) finden Sie in den entsprechenden separaten Lernmodulen.*
