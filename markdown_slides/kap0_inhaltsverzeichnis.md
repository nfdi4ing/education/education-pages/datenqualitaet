## Inhaltsverzeichnis

1. [Ziele des Trainings](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenqualitaet/html_slides/datenqualität.html#/2)
2. [Warum ist Datenqualität für meine Forschung wichtig?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenqualitaet/html_slides/datenqualität.html#/3)
3. [Nach welchen Kriterien sollten Daten selektiert werden?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenqualitaet/html_slides/datenqualität.html#/4) <br>
4. [Wodurch kann ich die Datenqualität fremder Daten überprüfen?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenqualitaet/html_slides/datenqualität.html#/5) <br>
    4.1 [Existiert eine Datendokumentation (vorhandene Metadaten)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenqualitaet/html_slides/datenqualität.html#/5)
    4.2 [Sind die Daten technisch nachnutzbar (Datenformate/Software)?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenqualitaet/html_slides/datenqualität.html#/5/4)
