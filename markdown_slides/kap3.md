## 3. Wodurch kann ich die Datenqualität fremder Daten überprüfen? 
## - 3.1. Existiert eine Datendokumentation (vorhandene Metadaten)?

### Überprüfung der Datenqualität

Für die Nachnutzung von Forschungsdaten ist vor allem die Qualität der Daten entscheidend. Folgende Aspekte sollten vor der Nachnutzung geklärt sein:

* Qualität der Daten muss für das Forschungsvorhaben ausreichend sein
* Elementar sind dabei:
    * Datenvollständigkeit  und Datenrichtigkeit
        * Vollständigkeit muss dem Projektziel genügen
        * Provinienz und Rechte müssen geklärt sein
        * Interpretierbarkeit (Dokumentation vorhanden?) muss möglich sein

    * Technische Nachnutzbarkeit 
        * Datenformate und/oder Software müssen nachnutzbar sein

<img align="right" src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/karikatur_überprüfung.png" alt="">


**Folgende Kriterien sollten bei der Qualitätsbewertung fremder Daten entscheidend sein**:

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/überprüfung_datenqualität.png" alt="">


### Allgemeine Datenqualitätsmetriken

Die Bewertung der Nützlichkeit und Relevanz von Datensätzen oder gesammelten Daten erfolgt durch die Anwendung von Datenqualitätsmetriken. 
Von den aufgelisteten Kriterien werden alle nur in den seltensten Fällen verwendet. Die Auswahl der Metriken richtet sich nach dem Datensatz und dem spezifischen Anwendungsfall. Erfahrungsgemäß kommen dabei vor allem die ersten sechs Kriterien für die Datenanalyse zum Einsatz.

[Auf der Webseite von NFDI4Ing über Datenqualitätsmetriken](https://data-quality-metrics.readthedocs.io/en/main/index.html) finden Sie eine detaillierte Beschreibung der genannten Merkmale sowie Informationen zu Tools und Bibliotheken, die verwendet werden können, um diese Metriken praktisch anzuwenden.

* **Completeness - Vollständigkeit**
    Gemessene, gespeicherte oder aufgezeichnete Daten müssen alle erforderlichen Attribute enthalten. NaN-Werte, die aus fehlerhaften Operationen resultieren, verringern die Vollständigkeit. Um die Qualität zu verbessern, sollten die Daten auf Vollständigkeit überprüft werden, um sicherzustellen, dass keine Informationen fehlen.

* **Unambiguous - Eindeutigkeit**
    Jeder Datensatz muss eindeutig interpretierbar sein. Wenn sich Einträge nur durch ein Merkmal oder nur durch die ID unterscheiden, ist eine Doppelanalyse vorzuziehen, da begründete Zweifel bestehen, dass es sich nicht um denselben Eintrag handelt. 

* **Correctness - Korrektheit**
    Daten sollten die Realität so genau wie möglich widerspiegeln und Messdaten sollten einen möglichst geringen Fehlerwert aufweisen. Anomalien in Datensätzen können kritisch für Unternehmen sein, da sie zu voreiligen Schlussfolgerungen und Entscheidungen führen können. Um Anomalien zu beseitigen, müssen Ausreißer identifiziert werden. Ausreißer sind Werte, die erheblich vom erwarteten Wert abweichen und Durchschnittswerte sowie die Standardabweichung beeinflussen. 

* **Timeliness - Aktualität**
    Alle Datensätze müssen dem aktuellen Zustand der abgebildeten Realität entsprechen, um die Forschungsfrage korrekt zu beantworten. Veraltete Daten können die Ergebnisse stark negativ beeinflussen. Z.B können Modelle für maschinelles Lernen, die auf alten Datensätzen trainiert wurden, fehlerhafte Ergebnisse liefern oder zu falschen Schlussfolgerungen führen. Zur Messung der Aktualität kann eine Stichprobe von „goldenen Datensätzen“ verwendet werden, für die die aktuellen Werte bekannt sind.


* **Accuracy - Genauigkeit**
    Genauigkeit: Datengenauigkeit ist ein wesentliches Merkmal der Datenqualität und bezieht sich darauf, dass Werte für ein bestimmtes Objekt im richtigen Format und in der richtigen Größe abgebildet werden. Diese Metrik betont die Korrektheit und Konsistenz der Daten, um Mehrdeutigkeit zu vermeiden. Genauigkeit umfasst Aspekte wie die Nachkommastellen von Dezimalzahlen oder die korrekte Schreibweise von Namen und Adressen. Sie kann durch Ground Truth oder alternative Datenerfassungen bestimmt und durch den Standardfehler beschrieben werden.

* **Consistency - Konsistenz**
    Ein Datensatz darf keine Widersprüche in sich selbst oder mit anderen Datensätzen aufweisen. Daten sind inkonsistent, wenn verschiedene gültige Zustände nicht miteinander vereinbar sind. Konsistenz misst, ob zwei Datenwerte aus verschiedenen Datensätzen nicht im Konflikt stehen. Der Prozentsatz der übereinstimmenden Werte in verschiedenen Datensätzen ist eine gängige Datenqualitätskennzahl. Konsistenz bezieht sich in erster Linie auf die Verwendung von Daten durch verschiedene Benutzer. Beispiele für konsistente Daten beziehen sich in der Regel auf Datenformate und Datentypen, die durchgängig identisch sein sollten, um ein bestimmtes Niveau an Datenqualität zu gewährleisten. Inkonsistenzen können durch zeitliche Veränderungen oder Variablen wie Jahrgänge, Einheiten, Genauigkeit, Vollständigkeit und Ein-/Ausschlussarten entstehen.

* **Freedom from redundancy - Freiheit von Redundanz**
    Es ist wichtig, doppelte Daten zu erkennen, was äußerst schwierig sein kann. Bei numerischen Messdaten ist es fast unmöglich, doppelte Nummern zu erkennen. Daher ist es besser, komplette Datenreihen zu vergleichen und einzeln zu entscheiden, ob es sich um eine doppelte Aufzeichnung handelt.


* **Relevance - Relevanz**
    Die Relevanz der Datenqualität bezieht sich auf die Nützlichkeit der erfassten Daten und darauf, ob diese Daten für die weitere Verarbeitung benötigt werden. Das Konzept der „Relevanz“ kann jedoch je nach dem spezifischen Kontext und den Anforderungen Ihrer Aufgabe variieren.
    Beispielsweise kann ein Datensatz auf ein Kriterium, d.h. ein Wort oder einen numerischen Wert, untersucht werden. Wenn der Datensatz das Kriterium enthält, wird er als relevant eingestuft.

* **Uniformity - Einheitlichkeit**
    Die Informationen eines Datensatzes müssen auf einheitliche Weise strukturiert sein. Daten desselben Typs sollten auch die gleichen Dimensionen haben.Einheitlichkeit ist daher spezifisch für Metriken und Maßeinheiten und ist besonders wichtig, wenn die Daten aus unterschiedlichen Quellen stammen.

* **Reliability - Zuverlässigkeit**
    Die Zuverlässigkeit der Daten bezieht sich auf die Herkunft der Daten und die Vertrauenswürdigkeit der Quelle sowie auf intrinsische Qualitätsmerkmale wie Vollständigkeit, Genauigkeit und Redundanzfreiheit. Sie ist eine Zusammenfassung dieser Qualitätsmerkmale und kann nur indirekt bestimmt werden. Die Zuverlässigkeit sollte vom Forscher im Vorfeld klar definiert werden, oft als Kombination aus Genauigkeit, Vollständigkeit und Konsistenz, wobei die Gewichtung je nach Bedarf des jeweiligen Forschers variiert.


### Checkliste zur Überprüfung der Datenqualität

#### Existiert eine Datendokumentation?

Informationen zum Entstehungskontext des Datensatzes sind von entscheidender Bedeutung, um den Datensatz nachnutzen zu können. Datenprovinienz* findet sich meist in einer separaten Datei (README) oder in den Metadaten* (festes Schema).

Checkliste dazu:

* **Titel**: Name des Datenmaterials oder Forschungsprojektes, in dessen Rahmen die Daten generiert wurden

* **Autor/Ersterheber**: Als Datenerheber können sowohl einzelne Person(-en) als auch ihre arbeitgebende(-n) Institution(-en) angegeben werden. Zur korrekten und eindeutigen Zuordnung eines Namens zu einer Person oder Institution werden in der Regel Normdaten aber auch persistente Identifikatoren (bspw. OrcID) eingesetzt.

* **Strategien der Datenerhebung**: Es kann sich dabei entweder um die Verarbeitung und/oder Interpretation von bereits existierendem (Sekundärdatenerhebung) Datenmaterial handeln. 

**„Datenprovenienz beantwortet die Fragen, warum und wie die Daten produziert wurden, wo, wann und von wem" aus Erklärt! Daten-Provenienz*

*[*Mehr zu Datendokumentation und zu Metadaten finden Sie im separaten Lernmodul "Metadaten"!*](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/Metadaten/html_slides/metadaten.html)


* **zeitliche Einordnung**: Angaben zu Zeiträumen, die mit den Daten in Verbindung stehen. Dazu gehören unter anderem der Zeitraum der Datenerhebung, die Mess- und Beobachtungszeit, Publikationsdatum oder auch der Projektbeginn und das Projektende.

* **Ort**: Bezug zu einem physischen Ort (zum Beispiel abgebildeter Raum) oder einer räumlichen Abdeckung (zum Beispiel Ort der Entstehung). Zur Genauigkeit von Ortsangaben werden gewöhnlich Normdaten verwendet.

* **Referenzierung**: Verweise auf andere Ressourcen (auch Messgeräte und Instrumente), mit denen die Daten in Verbindung stehen. Hierzu zählen unter anderem Angaben zur zugrunde liegenden Literatur, zu Rohdaten oder zugehörigen wissenschaftlichen Publikationen (zum Beispiel Artikel, Dissertation). Eine eindeutige Referenzierung erfolgt in der Regel mithilfe von persistenten Identifikatoren (PIDs). 


## 3.2. Sind die Daten technisch nachnutzbar (Datenformate/Software)?

### Technische Datenqualität

#### Sind die Daten technisch nachnutzbar?

* **Liegen gefundene Daten in einer Form vor, in der ich sie weiterverarbeiten kann?**
    * Proprietäre Datenformate können problematisch sein
    * Besser sind offene Dateiformate
    * Eine ausführliche Liste an vertraulichen Dateiformaten und Empfehlungen zur Konvertierung kann bei der weiteren Bearbeitung der Daten hilfreich sein.
    * Auf der nachfolgenden Folie sind die Eigenschaften langlebiger Formate aufgeführt.

*[*Mehr zu passenden Datenformaten finden Sie im separaten Lernmodul "Datenformate"!*](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenformate/html_slides/beispielvorlage.html)


#### Eigenschaften für langlebige Formate

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/eigenschaften_langlebige_formate.png" alt="">


#### Sind die Datenformate langfristig nutzbar?

* Für eine langfristige Nutzung eignen sich insbesondere vertrauenswürdige Dateiformate wie z.B.

* Tagged Image File Formaz (TIFF, TIF) für Bilder
* Plain text document (TXT, ASC) für Dokumente
* Portable Document Format/A (PDF/A) für Dokumente
* Waveform Audio File Format (WAV) für Audiodateien
* Extensible Markup Language (XML) für Dokumente

[Eine Übersicht über die Interoperabilität und Eignung verschiedener Dateiformate befindet sich im Trainingsmodul "Datenformate".](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenformate/html_slides/beispielvorlage.html#/5/3)


#### Sind die Daten technisch nachnutzbar?

Software-Programme und Dateiformate sind miteinander eng verknüpft. Die meisten Software-Programme können nur eine begrenzte Anzahl von Formaten öffnen und bearbeiten. 

* **Habe ich die passende Software zum Öffnen und Bearbeiten der vorgefundenen Dateiformate?**
* Bei der Auswahl der Software sind folgende Aspekte zu beachten:
    * Lizenzpflichtige versus Open-Source-Software
    * Software zum Weiterverarbeiten der Dateiformate (kann bspw. Datei öffnen aber nicht weiterverarbeiten)
    * Veraltete Dateiformate (neuere Programmversionen können evtl. ältere Daten nicht mehr lesen)

Wenn die Möglichkeiten, der Ihnen zur Verfügung stehenden Programme zum Interpretieren der aufgefundenen Dateiformate nicht ausreichen, müssen Sie ein Alternativprogramm finden.


### Beispiel zur Problematik veralteter Dateien

#### Veraltete Dateiformate

Dieses Beispiel aus dem Training "Datenformate" verdeutlicht die Probleme und Folgen, die veraltete Dateiformate verursachen können.

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Beispiel_Covid.png "Covid")

[https://www.bbc.com/news/technology-54423988](https://www.bbc.com/news/technology-54423988)


### Beispiel schwierige Weiterverarbeitung

#### In welcher strukturierten Form wurden die Daten erfasst?

Abhängig von der Form, in der die Daten erfasst wurden, können diese trotz vorliegender passender Software nicht nachnutzbar sein.

* Beispielweise lassen sich einzelne Inhalte einer, mithilfe von Microsoft Word erstellten, Tabelle nur schwer neu sortieren. 

    * Daher empfiehlt sich die Konvertierung der Word-Tabelle in eine strukturierte EXCEL-Tabelle, deren Inhalte sich gruppieren und zusammenfassen lassen. 

    * Die Übertragung der Daten ist meist nur manuell möglich (zum Beispiel durch das Kopieren mit Steuerungstaste + C / Steuerungstaste + V). 

        * Bei großen Datenmengen zu zeitaufwendig und unübersichtlich.


### Weitere Hürden

#### Liegen die gefundenen Daten in einer Form vor, in der ich sie weiterverarbeiten kann?

* Erfordert die Beschaffung von Software oder die Überführung der Daten in eine den projektspezifischen Bedürfnissen angepasste Form einen hohen Aufwand?

    * In solchen Fällen gehen Sie wie folgt vor:

        * Suchen Sie nach geeigneten Daten in einem anderen Repositorium.
        * Kontaktieren Sie die Autoren beziehungsweise Ersterheber der Daten. 
            * Datenautoren sind ggfs. im Besitz alternativer Dateiversionen bzw. Datenformaten.