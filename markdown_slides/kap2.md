## 2. Nach welchen Kriterien sollten Daten selektiert werden?

### Datenauswahl

#### Welche Kriterien kommen in Frage?

* **Dokumentation**: Nachvollziehbarkeit für Verantwortungszwecke 
    * Bestimmen der rechtlichen oder anderen Pflichten (z.B. Grundlage einer Publikation, Datenschutz, Vertraulichkeit, Aufbewahrungsdauer)

* **Nachnutzung**: Zitierfähigkeit, erneute wissenschaftliche Nutzung, Bewahrung nicht reproduzierbarer Daten
    * Bestimmen des Wertes und intellektuelle Bewertung (Potential für Nachnutzung und welche Institution ist der beste Aufbewahrungsort?)
    * Bestimmen der Machbarkeit (Welche Technik und welches Kontextwissen ist notwendig? Können diese Anforderungen zumindest überhaupt gewährleistet werden? Sind die Daten ausreichend dokumentiert?)

* **verbesserte Datennutzung in Projekten**: Daten verändern sich noch und gehen am Ende eines Projekts in einen anderen Kontext über
    * Daten müssen zwar bereits im Projekt entsprechend eines bestimmten Service Levels aufbewahrt werden, aber am Ende neu bewertet werden


#### Datenselektion hängt von der potentiellen Nachnutzung ab

* Weitere Publikation: referenzierte (=verarbeitete) Daten mit zusätzlicher Dokumentation

* Verifikation: Referenzierte Daten inklusive Analyseschritte

* Weitere Analysen: alle Originaldaten

* Lernen und Lehren: Proben von Originaldaten und zusammengestellten Daten inklusive Analyseschritte


#### Hier sind verschiedene Nachnutzungsszenarien visualisiert, für die eine gute Dokumentation ausschlaggebend sind

<img align="middle" src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/datenauswahl.png" alt="">


#### Welche Datentypen kommen in Frage?

* Primärdaten (Datenquelle)
    * Daten, die originär gesammelt oder erstellt wurden 
* Zusammengestellte Datensätze 
    * Daten, die aus den eigenen oder fremden Datenquellen extrahiert oder abgeleitet wurden
* Referenzierte Daten
    * Daten, die aus einer Teilmenge der Primärdaten verarbeitet wurden, um die Analyse weiterzuverfolgen oder daraus Schlüsse zu ziehen

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
CC0/forschungsdaten.info
<br>
Vgl. auch:
https://www.forschungsdaten.info/themen/veroeffentlichen-und-archivieren/datenvalidierung/ 


#### Was behalten? Was kann weg?

<img align="right" src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/kriterien_datenselektion.png" alt="">

* Aufbewahren:
    * Bewahrung von Präsentations- und Analyseumgebungen
    * Dokumentation von Annotations- und Analyseprozessen
    * Welche Version(en) davon?

* Verzichtbar (löschen):
    * technisch bedingte Strukturen und Daten
    * leere Datensätze / Tabellen
    * Duplikate
    * unvollständige Datensätze

<br>
<br>
<br>
<br>
<br>
<br>

Vgl. auch:
https://www.dcc.ac.uk/sites/default/files/documents/How%20to%20Appraise%20and%20Select%20Research%20Data.pdf


### Datenqualitätsworkflow

#### Beispiel eines möglichen Datenworkflows eines Instituts

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/workflow.png" alt="">

Quelle: DINI/nestor-Workshop "Forschungsdaten auswählen und bewerten"


### Datenselektion Publikation vs. Archivierung

#### Was sind sinnvolle Beiträge zur Publikation? Was ist archivierungswert? 
**Folgende Fragen und Aspekte können bei einer Entscheidung hilfreich sein:**

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/fragen_archivierung_publikation.png" alt="">


**Hier sind ein paar Beispiele aufgeführt, die in Frage kämen.**

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenqualitaet/-/raw/master/media_files/archivierung_publikation.png" alt="">


Datenselektion hängt natürlich auch vom Aufwand ab. Insbesondere für die Langzeitarchivierung (LZA)* stehen bspw. einfach wiederholbare oder kostengünstige Experimente gegenüber von teuren, seltenen, teilw. schwierig zu erhaltenen Daten.

* Folgende Schritte müssen für eine LZA bedacht und durchgeführt werden:
    * Metadatentemplate erstellen
    * Datenauswahl bereitstellen
    * Metadaten ergänzen 
    * Datentransfer zum Repositorium 
    * Zugang zu fremdfinanzierten Speicherinfrastrukturen

* Zur LZA-Überprüfung kann das Tool JHOVE genutzt werden (ein erweiterbares Software-Framework für die Identifizierung, Validierung und Charakterisierung von Formaten digitaler Objekte)

<br>
<br>
<br>
<br>

Crashkurs Digitale Langzeitarchivierung - Dateiformate
https://zenodo.org/record/3985075#.YbTPdNXMKWV  

[**Mehr zur Archivierung finden Sie im separaten Lernmodul "Daten archivieren"!*](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dlc-datalifecycle/html_slides/dlc5.html#/)